# Torque3D grkb Edition

MIT Licensed Open Source version of [Torque 3D](http://torque3d.org) from [GarageGames](http://www.garagegames.com)

# What's new
- urlEnc and urlDec functions
- Platform related functions (isWindows, isMacintosh, isLinux)
- guiSwatchCtrl (colorable just like in Blockland!)
- updated libpng

# License 

All assets and code are under the [![license](https://img.shields.io/github/license/GarageGames/Torque3D.svg)](https://github.com/GarageGames/Torque3D/blob/master/LICENSE.md)
